import { createPanel } from './panelHelpers';
import PanelA from '../components/PanelA';

export default (parent = null, data) => {
  const flow = createPanel(PanelA)(parent, data);
};
