/* eslint-disable import/default */

import 'babel-polyfill';
import nflow from 'nflow';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import cuid from 'cuid';
import configureStore from './store/configureStore';
import PanelCounter from './components/PanelCounter';
import * as panels from './panels';
import './favicon.ico';
import './styles/styles.scss';

const mapFlowToRedux = {
  'common-data': ({ data }) => ({ type: 'COMMON_DATA', data })
};

const mapReduxToFlow = {
  ADD_ITEM: ({ name }) => ({ type: 'add-item', payload: name }),
  DELETE_ITEM: ({ id }) => ({ type: 'delete-item', payload: id })
};

const canvasFlow = nflow.create('canvas');
const store = configureStore(canvasFlow, mapFlowToRedux, mapReduxToFlow);

createCanvas(canvasFlow);

function createCanvas(flow) {
  flow
    .data({ element: document.getElementById('panels') })
    .on('add-panel', addPanel);

  renderPanelCounter();
  initPanels();

  createCommonDataService(flow);

  function addPanel(panelType, element = null) {
    if (element == null) {
      element = document.createElement('div');
      flow.data().element.appendChild(element);
    }

    console.log(`Adding panel of type ${panelType}`);
    const Panel = panels[panelType];

    Panel(flow, { element, store, panelId: cuid(), panelType });
  }
}

function renderPanelCounter() {
  render(<Provider store={store}><PanelCounter /></Provider>, document.getElementById('panel-counter'));
}

function initPanels() {
  document.getElementById('panels').childNodes.forEach(element => {
    const panelType = element.getAttribute('data-panel-type');
    nflow.emit('add-panel', panelType, element);
  });

  document.getElementById('add-panel').addEventListener('click', () => {
    const panelType = document.getElementById('panel-type').value;
    nflow.emit('add-panel', panelType);
  });
}

function createCommonDataService(parent = null) {
  const flow = nflow.create('common-data-service')
    .parent(parent)
    .data([
      { id: 10, name: 'fish' },
      { id: 20, name: 'frog' },
      { id: 30, name: 'newt' }
    ])
    .on('add-item', addItem)
    .on('delete-item', deleteItem);

  flow.emit('common-data', { data: flow.data() });

  function addItem(name) {
    flow.data().push({ id: cuid(), name });
    flow.emit('common-data', { data: flow.data() });
  }

  function deleteItem (id) {
    flow.data(flow.data().filter(item => item.id !== id));
    flow.emit('common-data', { data: flow.data() });
  }
}
