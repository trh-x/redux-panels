import React, { PropTypes } from 'react';
import { panelConnect, panelCombineReducers } from '../reactReduxPanel';
import { incrementPanelCount } from '../shared/actions';

// COMPONENT

class PanelB extends React.Component {
  componentDidMount() {
    this.props.incrementPanelCount();
  }

  render() {
    const { byId, allIds = [] } = this.props.data;
    return (
      <div>
        <h4>PanelB {this.props.name} :: {this.props.filter}</h4>
        <input onChange={({ target: { value } }) => this.props.setFilter(value)} />
        { allIds.filter(id => byId[id].name.match(RegExp(this.props.filter))).map(id => <p key={id}>{byId[id].name}</p>) }
      </div>
    );
  }
}

// REDUCERS

const filterReducer = (state = '', action) => {
  switch (action.type) {
    case 'SET_FILTER':
      return action.filter;
  }
  return state;
};

const reducer = panelCombineReducers({
  filter: filterReducer
});

// ACTIONS

const setFilter = filter => ({ type: 'SET_FILTER', filter });

// CONTAINER

const PanelComponent = panelConnect(
  ({ global: { common: { data } }, instance: { filter } }) => ({ data, filter }),
  { global: { incrementPanelCount }, instance: { setFilter } }
)(PanelB);

PanelComponent.reducer = reducer;

export default PanelComponent;
