import { combineReducers } from 'redux';
import { keyBy, property } from 'lodash/fp';
import { rootPanelReducer } from '../reactReduxPanel';

const commonReducer = (state = { data: {}, panelCount: 0 }, action) => {
  switch (action.type) {
    case 'COMMON_DATA':
      console.log('COMMON_DATA', action);
      return {
        ...state,
        data: {
          byId: keyBy('id', action.data),
          allIds: action.data.map(property('id'))
        }
      };
    case 'PANEL_COUNT_INC':
      return {
        ...state,
        panelCount: state.panelCount + 1
      };
  }
  return state;
};

const rootReducer = combineReducers({
  common: commonReducer,
  panels: rootPanelReducer
});

export default rootReducer;
