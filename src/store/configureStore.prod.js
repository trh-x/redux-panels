// This file merely configures the store for hot reloading.
// This boilerplate file is likely to be the same for each project that uses Redux.
// With Redux, the actual stores are in /reducers.

import {applyMiddleware, createStore} from 'redux';
import rootReducer from '../reducers';
import { panelMiddleware } from '../reactReduxPanel';
import { createFlowMiddleware } from '../reduxFlow';

const initialState = {};

export default function configureStore(flow, mapFlowToRedux, mapReduxToFlow) {
  const flowMiddleware = createFlowMiddleware(flow, mapFlowToRedux, mapReduxToFlow);

  const store = applyMiddleware(panelMiddleware, flowMiddleware)(createStore)(rootReducer, initialState);
  return store;
}
