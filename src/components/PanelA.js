import React, { PropTypes } from 'react';
import { panelConnect, panelCombineReducers } from '../reactReduxPanel';
import { incrementPanelCount } from '../shared/actions';
import DataList from './DataList';

// COMPONENT

class PanelA extends React.Component {
  componentDidMount() {
    setInterval(() => {
      this.props.incrementCounter();
    }, 3000 * Math.random());

    this.props.incrementPanelCount();
  }

  render() {
    return (
      <div>
        <h4>PanelA {this.props.name} :: {this.props.counter}</h4>
        <DataList />
      </div>
    );
  }
}

// REDUCERS

const counterReducer = (state = Math.floor(Math.random() * 100), action) => {
  switch (action.type) {
    case 'COUNTER_INC':
      return state + 1;
  }
  return state;
};

const reducer = panelCombineReducers({
  counter: counterReducer
});

// ACTIONS

const incrementCounter = () => ({ type: 'COUNTER_INC' });

// CONTAINER

const PanelComponent = panelConnect(
  ({ instance: { counter } }) => ({ counter }),
  { global: { incrementPanelCount }, instance: { incrementCounter } }
)(PanelA);

PanelComponent.reducer = reducer;

export default PanelComponent;

// TODO: Shared state across panel type?
