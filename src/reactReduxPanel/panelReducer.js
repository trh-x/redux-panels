import { combineReducers } from 'redux';

const panelReducers = {};

export const rootPanelReducer = (state = {}, action) => {
  const { panelType } = action;

  if (panelReducers[panelType]) {
    return {...state, [panelType]: panelReducers[panelType](state[panelType], action)};
  }
  return state;
};

export const addPanelReducer = (panelType, reducer) => { panelReducers[panelType] = reducer; }

export const panelCombineReducers = reducers => {
  const combinedReducers = combineReducers(reducers);
  return (state = {}, action) => {
    const instanceState = state[action.panelId] || {};
    return {
      ...state,
      [action.panelId]: combinedReducers(instanceState, action)
    };
  };
};
