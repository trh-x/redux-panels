import React, { PropTypes } from 'react';

export class PanelProvider extends React.Component {
  getChildContext() {
    const { store, panelId, panelType } = this.props;
    return { store, panelId, panelType };
  }

  render() {
    return this.props.children;
  }
}

PanelProvider.childContextTypes = {
  store: PropTypes.object, panelId: PropTypes.string, panelType: PropTypes.string
};
