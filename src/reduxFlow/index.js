const connectFlow = (store, flow, mapFlowToRedux) => {
  Object.keys(mapFlowToRedux).forEach(eventName => {
    const actionCreator = mapFlowToRedux[eventName];
    flow.on(eventName, data => store.dispatch(actionCreator(data)));
  });
};

export const createFlowMiddleware = (flow, mapFlowToRedux = {}, mapReduxToFlow = {}) => {
  const flowMiddleware = store => {
    connectFlow(store, flow, mapFlowToRedux);

    return next => action => {
      const eventCreator = mapReduxToFlow[action.type];
      if (eventCreator) {
        const flowEvent = eventCreator(action);
        flow.emit(flowEvent.type, flowEvent.payload);
      }
      next(action);
    };
  };

  return flowMiddleware;
};
