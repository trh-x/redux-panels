import React, { PropTypes } from 'react';
import { bindActionCreators, combineReducers } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash/fp';

const createPanelWrapper = PanelComponent => {
  const PanelWrapper = (props, { panelId, panelType }) => {
    return <PanelComponent {...props} panelId={panelId} panelType={panelType} />;
  };
  PanelWrapper.contextTypes = { panelId: PropTypes.string, panelType: PropTypes.string };
  return PanelWrapper;
};

const augmentInstanceActionProps = ({...actionCreators}, { panelType, panelId }) => (
  Object.keys(actionCreators).reduce((instanceActionCreators, key) => {
    instanceActionCreators[key] = (...args) => {
      const action = actionCreators[key](...args);
      action.panelType = panelType;
      action.panelId = panelId;
      return action;
    };
    return instanceActionCreators;
  }, {})
);

const emptyObj = {};

export const panelConnect = (mapPanelStateToProps, mapPanelDispatchToProps) => Panel => {
  const PanelComponent = ({ global, instance = emptyObj, ...props }, { store }) => {
    const panelProps = mapPanelStateToProps({ global, instance });

    const globalActionProps = mapPanelDispatchToProps.global
      ? bindActionCreators(mapPanelDispatchToProps.global, store.dispatch)
      : emptyObj;

    const instanceActionProps = mapPanelDispatchToProps.instance
      ? bindActionCreators(augmentInstanceActionProps(mapPanelDispatchToProps.instance, props), store.dispatch)
      : emptyObj;

    return (
      <Panel name={`PANEL_${props.panelId}`}
        {...props}
        {...panelProps}
        {...globalActionProps}
        {...instanceActionProps}
        />
    );
  };

  PanelComponent.contextTypes = { store: PropTypes.object };

  return createPanelWrapper(connect(
    (state, props) => {
      return {
        global: state,
        instance: get(`panels.${props.panelType}.${props.panelId}`, state)
      };
    }
  )(PanelComponent));
};


