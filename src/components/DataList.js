import React, { PropTypes } from 'react';
import { panelConnect } from '../reactReduxPanel';

// COMPONENT

class DataList extends React.Component {
  render() {
    const { data: { byId, allIds = [] }, counter } = this.props;
    return (
      <div>
        { allIds.map(id => <p key={id}>{byId[id].name}<button onClick={() => this.props.deleteItem(id)}>DEL</button></p>) }
        <span>{counter}</span>
      </div>
    );
  }
}

// ACTIONS

const deleteItem = id => ({ type: 'DELETE_ITEM', id });

// CONTAINER

const PanelComponent = panelConnect(
  ({ global: { common: { data } }, instance: { counter } }) => ({ data, counter }),
  { global: { deleteItem } }
)(DataList);

export default PanelComponent;
