import { addPanelReducer } from './panelReducer';

export const panelMiddleware = store => next => action => {
  if (action.type == 'REGISTER_PANEL') {
    addPanelReducer(action.panelType, action.reducer);
  }
  else next(action);
}
