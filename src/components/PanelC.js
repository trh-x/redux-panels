import React, { PropTypes } from 'react';
import { panelConnect, panelCombineReducers } from '../reactReduxPanel';
import { incrementPanelCount } from '../shared/actions';

// COMPONENT

class PanelC extends React.Component {
  componentDidMount() {
    this.props.incrementPanelCount();
  }

  _stashInput = input => { this._input = input; }

  _addItem = () => this.props.addItem(this._input.value);

  render() {
    return (
      <div>
        <h4>PanelC {this.props.name}</h4>
        <input ref={this._stashInput} />
        <button onClick={this._addItem}>Add</button>
      </div>
    );
  }
}

// ACTIONS

const addItem = name => ({ type: 'ADD_ITEM', name });

// CONTAINER

const PanelComponent = panelConnect(
  () => {},
  { global: { incrementPanelCount }, instance: { addItem } }
)(PanelC)

// PanelComponent.reducer = reducer;

export default PanelComponent;
