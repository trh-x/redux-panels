import React from 'react';
import { connect } from 'react-redux';

const PanelCounter = ({ panelCount }) => <span>{panelCount} panels. </span>;

export default connect(({ common: { panelCount } }) => ({ panelCount }))(PanelCounter);
