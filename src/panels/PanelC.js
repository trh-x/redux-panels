import { createPanel } from './panelHelpers';
import PanelC from '../components/PanelC';

export default (parent = null, data) => {
  const flow = createPanel(PanelC)(parent, data);
};
