import nflow from 'nflow';
import React from 'react';
import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
import { PanelProvider } from '../reactReduxPanel';

export const createPanel = Panel => (parent = null, { element, store, panelId, panelType }) => {
  const flow = nflow.create(panelType)
    .parent(parent)
    .data({ element, store, panelId, panelType });

  render(flow, Panel);

  return flow;
};

function render(flow, Panel) {
  const { element, store, panelId, panelType } = flow.data();

  if (Panel.reducer) store.dispatch({
    type: 'REGISTER_PANEL',
    panelType,
    reducer: Panel.reducer
  });

  ReactDOM.render(
    <PanelProvider store={store} panelId={panelId} panelType={panelType}>
      <Panel />
    </PanelProvider>, element
  );
}
