import { createPanel } from './panelHelpers';
import PanelB from '../components/PanelB';

export default (parent = null, data) => {
  const flow = createPanel(PanelB)(parent, data);
};
